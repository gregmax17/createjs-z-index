# CreateJS Z-Index

Implements z-index to the children in a container.

Adds a `zIndex` property to the `createjs.DisplayObject` object, and a `enableZIndex` property to `createjs.Container` object. By default, the `enableZIndex` property value is `true`, so whenever another Display Object/child is added to that Container it will sort by the `zIndex` value. The lower this `zIndex` value is, the further in the background it will be placed (be drawn first).

If two Display Objects have the same `zIndex` it will compare their `id` property value. This `id` property value is set every time an instance of the Display Object is instantiated, and incremented everytime a new Display Object is created. For example, if you have created two sprites, the first sprite created would have an `id` of 1, and the second would have an `id` of 2, respectively. So if both of these sprites have the same `zIndex` value the first sprite will be drawn then the second.