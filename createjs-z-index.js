// add zIndex property, and a set/get listener
createjs.DisplayObject.prototype._zIndex = 0;
Object.defineProperties(createjs.DisplayObject.prototype, {
	zIndex: {
		get: function () {
			return this._zIndex;
		},
		set: function (zIndex) {
			this._zIndex = zIndex;
			if (this.parent instanceof createjs.Container && this.parent.enableZIndex) {
				this.parent.sortChildren(this.parent._sortChildrenZIndex);
			}
		}
	}
});

// when we add a child to the stage, we need to sort the children
createjs.Container.prototype.addChild = function (child) {
	if (child == null) { return child; }
	var l = arguments.length;
	if (l > 1) {
		for (var i = 0; i < l; i++) { this.addChild(arguments[i]); }
		return arguments[l - 1];
	}
	// Note: a lot of duplication with addChildAt, but push is WAY faster than splice.
	var par = child.parent, silent = par === this;
	par && par._removeChildAt(createjs.indexOf(par.children, child), silent);
	child.parent = this;
	this.children.push(child);

	// added these lines
	if (this.enableZIndex) {
		this.sortChildren(this._sortChildrenZIndex);
	}

	if (!silent) { child.dispatchEvent("added"); }
	return child;
};

// our new sort method that sorts by the `zIndex` - if `enableZIndex` is set to true
createjs.Container.prototype.enableZIndex = true;
createjs.Container.prototype._sortChildrenZIndex = function (a, b) {
	var order = a.zIndex - b.zIndex;
	if (!order) {
		order = a.id - b.id;
	}
	return order;
};